//
//  main.m
//  Neon
//
//  Created by Rene Cacheaux on 7/19/13.
//  Copyright (c) 2013 PieceOfCake Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NEAppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([NEAppDelegate class]));
  }
}
