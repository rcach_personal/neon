#import "NEAppDelegate.h"

#import "NEApplicationViewController.h"

@implementation NEAppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  self.window.backgroundColor = [UIColor whiteColor];
  self.window.rootViewController = [NEApplicationViewController new];
  [self.window makeKeyAndVisible];
  return YES;
}

@end
