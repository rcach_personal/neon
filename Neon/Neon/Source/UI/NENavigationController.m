#import "NENavigationController.h"

@interface NENavigationController ()

@end

@implementation NENavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self configure];
}

- (void)configure {
  self.navigationBar.barStyle = UIBarStyleBlackTranslucent;
  self.toolbar.barStyle = UIBarStyleBlackTranslucent;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

@end
