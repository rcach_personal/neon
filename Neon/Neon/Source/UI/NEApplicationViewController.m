#import "NEApplicationViewController.h"

#import "NEColors.h"
#import "NENavigationController.h"
#import "NEMailListViewController.h"

@interface NEApplicationViewController ()
@property(nonatomic, strong) NENavigationController *appNavController;
@end

@implementation NEApplicationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}

- (void)loadView{
  self.view = [UIView new];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self installNavController];
  [self configure];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  self.appNavController.view.frame = self.view.bounds;
}

- (void)configure {
  self.view.tintColor = [NEColors pink];
}

- (void)installNavController {
  self.appNavController =
      [[NENavigationController alloc] initWithRootViewController:[self newMailListViewController]];
  [self addChildViewController:self.appNavController];
  [self.view addSubview:self.appNavController.view];
}

- (NEMailListViewController *)newMailListViewController {
  return [[NEMailListViewController alloc]
          initWithCollectionViewLayout:[UICollectionViewFlowLayout new]];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

@end
