#import <Foundation/Foundation.h>

@interface NEColors : NSObject

+ (UIColor *)pink;
+ (UIColor *)blue;
+ (UIColor *)orange;
+ (UIColor *)green;
+ (UIColor *)purple;
+ (UIColor *)muskyGrey;

@end
