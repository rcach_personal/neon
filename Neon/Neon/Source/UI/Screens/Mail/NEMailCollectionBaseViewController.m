#import "NEMailCollectionBaseViewController.h"

#import "NEColors.h"
#import "NEMailCollectionBaseViewController+Protected.h"

@interface NEMailCollectionBaseViewController ()
@property(nonatomic, copy, readwrite) NSArray *messages;
@end

@implementation NEMailCollectionBaseViewController

- (id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout {
  self = [super initWithCollectionViewLayout:layout];
  if (self) {
    self.title = @"inbox";
    [self loadCollectionView];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [self configure];
  [self loadData];
}

- (void)loadCollectionView {
  NSAssert(NO, @"abstract");
}

- (void)configure {
  self.collectionView.backgroundColor = [NEColors muskyGrey];
  self.navigationItem.rightBarButtonItem =
      [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose
                                                    target:nil
                                                    action:NULL];

  UIBarButtonItem *organizeMailBarButtonItem =
      [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize
                                                    target:nil
                                                    action:NULL];
  self.navigationItem.leftBarButtonItem =
      [[UIBarButtonItem alloc] initWithTitle:@"settings"
                                       style:UIBarButtonItemStylePlain
                                      target:nil
                                      action:NULL];
  self.toolbarItems = @[[self newFlexibleBarButtonItem],
                        organizeMailBarButtonItem,
                        [self newFlexibleBarButtonItem]];
  self.navigationController.toolbarHidden = NO;
}

- (void)loadData {
  NSString *path = [[NSBundle mainBundle] pathForResource:@"Email" ofType:@"json"];
  NSData *data = [[NSFileManager defaultManager] contentsAtPath:path];
  self.messages = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
}
  
- (UIBarButtonItem *)newFlexibleBarButtonItem {
  return [[UIBarButtonItem alloc]
          initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
}

@end
