#import "NEMailCollectionBaseViewController.h"

@interface NEMailCollectionBaseViewController (Protected)

// List of fake email messages.
@property(nonatomic, copy, readonly) NSArray *messages;

// Configures the navigation bar, toolbar, and collection view.
- (void)configure;

// Abstract method. Subclasses must set up their collection view in this method.
- (void)loadCollectionView;

@end
