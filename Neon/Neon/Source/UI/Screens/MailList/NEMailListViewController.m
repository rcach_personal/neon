#import "NEMailListViewController.h"

#import "UIImage+ImageEffects.h"

#import "NEColors.h"
#import "NEMailFeedViewController.h"
#import "NEMailListCell.h"
#import "NEMailCollectionBaseViewController+Protected.h"

@interface NEMailListViewController ()<UICollectionViewDataSource>
@end

@implementation NEMailListViewController

- (void)loadCollectionView {
  [self.collectionView registerClass:[NEMailListCell class]
          forCellWithReuseIdentifier:[self cellReuseID]];
  UICollectionViewFlowLayout *flow =
      (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
  flow.itemSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, 140.0f);
  flow.minimumLineSpacing = 0.0f;
}

#pragma mark - Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return [self.messages count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  NEMailListCell *cell = (NEMailListCell *)
      [collectionView dequeueReusableCellWithReuseIdentifier:[self cellReuseID]
                                                forIndexPath:indexPath];
  [cell.webView loadHTMLString:self.messages[indexPath.item][@"html"] baseURL:nil];
  
  double delayInSeconds = 3.0;
  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    UIGraphicsBeginImageContext(cell.webView.frame.size);
    CGContextRef c = UIGraphicsGetCurrentContext();
    [cell.webView.layer renderInContext:c];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
//    cell.webViewSnapshot.image = image;
    cell.webViewSnapshot.image = [image applyDarkEffect];
  });
  
  
  cell.backgroundColor = [UIColor whiteColor];
  return cell;
}
  
- (void)collectionView:(UICollectionView *)collectionView
    didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//  NEMailListCell *cell = (NEMailListCell *)[collectionView cellForItemAtIndexPath:indexPath];
//  cell.webView.alpha = 0.0f;
//  [UIView animateWithDuration:1.0 animations:^{
//    cell.webViewSnapshot.alpha = 0.0f;
//    cell.webView.alpha = 1.0f;
//  }];

  NEMailFeedViewController *controller = [[NEMailFeedViewController alloc] initWithCollectionViewLayout:[UICollectionViewFlowLayout new]];
  controller.useLayoutToLayoutNavigationTransitions = YES;
  [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Strings

- (NSString *)cellReuseID {
  return @"Cell";
}

@end
