#import "NEMailListCell.h"

#import "NEColors.h"

@interface NEMailListCell ()
@property(nonatomic, strong, readwrite) UIWebView *webView;
@property(nonatomic, strong, readwrite) UIImageView *webViewSnapshot;
@end

@implementation NEMailListCell

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    _webView = [UIWebView new];
    _webView.scrollView.scrollEnabled = NO;
    _webView.userInteractionEnabled = NO;
//    _webView.scalesPageToFit = YES;
    [self.contentView addSubview:_webView];
    _webViewSnapshot = [UIImageView new];
    [self.contentView addSubview:_webViewSnapshot];
    self.layer.borderColor = [NEColors blue].CGColor;
    self.layer.borderWidth = 0.5f;
    
  }
  return self;
}
  
- (void)layoutSubviews {
  [super layoutSubviews];
  self.webView.frame = CGRectInset(self.contentView.bounds, -4.0f, 0.0f);
  self.webViewSnapshot.frame = CGRectInset(self.contentView.bounds, -4.0f, 0.0f);
}

@end
