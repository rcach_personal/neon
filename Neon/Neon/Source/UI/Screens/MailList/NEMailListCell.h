// The mail list cell.
@interface NEMailListCell : UICollectionViewCell
@property(nonatomic, strong, readonly) UIWebView *webView;
@property(nonatomic, strong, readonly) UIImageView *webViewSnapshot;
@end
