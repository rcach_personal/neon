#import "NEMailFeedCell.h"

#import "NEColors.h"

@interface NEMailFeedCell ()
@property(nonatomic, strong, readwrite) UIWebView *webView;
@end

@implementation NEMailFeedCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
      _webView = [UIWebView new];
      _webView.scrollView.scrollEnabled = NO;
      _webView.userInteractionEnabled = NO;
      _webView.scalesPageToFit = YES;
      [self.contentView addSubview:_webView];
      self.layer.borderColor = [NEColors blue].CGColor;
      self.layer.borderWidth = 0.5f;
    }
    return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  self.webView.frame = self.contentView.bounds;
}

@end
