#import "NEMailFeedViewController.h"

#import "NEColors.h"
#import "NEMailCollectionBaseViewController+Protected.h"
#import "NEMailFeedCell.h"

@interface NEMailFeedViewController ()<UICollectionViewDataSource>
@end

@implementation NEMailFeedViewController

- (void)loadCollectionView {
  [self.collectionView registerClass:[NEMailFeedCell class]
          forCellWithReuseIdentifier:[self cellReuseID]];
  UICollectionViewFlowLayout *flow =
      (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
  flow.itemSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, 400.0f);
  flow.minimumLineSpacing = 0.0f;
}

#pragma mark - Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return [self.messages count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  NEMailFeedCell *cell = (NEMailFeedCell *)
      [collectionView dequeueReusableCellWithReuseIdentifier:[self cellReuseID]
                                                forIndexPath:indexPath];
  [cell.webView loadHTMLString:self.messages[indexPath.item][@"html"] baseURL:nil];
  cell.backgroundColor = [UIColor whiteColor];
  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
    didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Strings

- (NSString *)cellReuseID {
  return @"FeedCell";
}
@end
