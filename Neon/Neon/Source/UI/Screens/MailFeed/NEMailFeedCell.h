// The mail feed cell.
@interface NEMailFeedCell : UICollectionViewCell

@property(nonatomic, strong, readonly) UIWebView *webView;

@end
