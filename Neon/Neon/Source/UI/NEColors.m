#import "NEColors.h"

@implementation NEColors

+ (UIColor *)pink {
  return [UIColor colorWithRed:255.0f/255.0f
                         green:73.0/255.0f
                          blue:129.0f/255.0f
                         alpha:1.0f];
}

+ (UIColor *)blue {
  return [UIColor colorWithRed:102.0f/255.0f
                         green:199.0/255.0f
                          blue:250.0f/255.0f
                         alpha:1.0f];
}

+ (UIColor *)orange {
  return [UIColor colorWithRed:255.0f/255.0f
                         green:140.0/255.0f
                          blue:0.0f/255.0f
                         alpha:1.0f];
}

+ (UIColor *)green {
  return [UIColor colorWithRed:12.0f/255.0f
                         green:211.0/255.0f
                          blue:24.0f/255.0f
                         alpha:1.0f];
}

// Other purple 163, 69, 254
+ (UIColor *)purple {
  return [UIColor colorWithRed:200.0f/255.0f
                         green:110.0/255.0f
                          blue:223.0f/255.0f
                         alpha:1.0f];
}
  
+ (UIColor *)muskyGrey {
  return [UIColor colorWithRed:43.0f/255.0f
                         green:43.0/255.0f
                          blue:43.0f/255.0f
                         alpha:1.0f];
}

@end
