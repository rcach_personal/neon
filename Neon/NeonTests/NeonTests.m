//
//  NeonTests.m
//  NeonTests
//
//  Created by Rene Cacheaux on 7/19/13.
//  Copyright (c) 2013 PieceOfCake Software. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface NeonTests : XCTestCase

@end

@implementation NeonTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
